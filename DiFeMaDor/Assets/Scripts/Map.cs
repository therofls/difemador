﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum MapType {
    Small,
    Medium,
    Big,
    Test
}
public class Map : MonoBehaviour
{
    [SerializeField] private string _mapName;
    [SerializeField] private MapType _currentType;
    [SerializeField] private MeshRenderer _mapMesh;
    
    private void Start()
    {
        Init(MapType.Test, new List<String> { "Green", "Blue" });
    }
    public void Init(MapType mapType, string materialName) {
        _currentType = mapType;
        _mapName = GetNameByType(mapType);
        _mapMesh.material = GetMaterialByString(materialName);
        gameObject.transform.localScale = GetScaleByType(mapType);
    }
    public void Init(MapType mapType, List<string> materialNames)
    {
        _currentType = mapType;
        _mapName = GetNameByType(mapType);
        GetMaterialsByList(_mapMesh, materialNames);
        gameObject.transform.localScale = GetScaleByType(mapType);
    }

    private Vector3 GetScaleByType(MapType mapType)
    {
        switch (mapType) {
            case MapType.Big:
                return new Vector3(50, 1, 50);
            case MapType.Medium:
                return new Vector3(25, 1, 25);
            case MapType.Small:
                return new Vector3(10, 1, 10);
            default:
                return new Vector3(5, 1, 5);
        }
    }

    private string GetNameByType(MapType mapType)
    {
        switch (mapType) {
            case MapType.Big: 
                return "BigMap";
            case MapType.Medium:
                return "MediumMap";
            case MapType.Small:
                return "SmallMap";
            default:
                return "TestMap";
        }
    }

    private Material GetMaterialByString(string materialName)
    {
        return Resources.Load("Materials/" + materialName, typeof(Material)) as Material;
    }

    private void GetMaterialsByList(MeshRenderer mapMesh, List<string> materialNames)
    {
        Material[] materials = new Material[materialNames.Count];
        for (int i = 0; i < materialNames.Count; i++) {
            materials[i] = GetMaterialByString(materialNames[i]);
        }
        mapMesh.materials = materials;
    }
}
